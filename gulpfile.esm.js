import { src } from 'gulp';
import yaml from 'js-yaml';
import through2 from 'through2';

export default () => {
  return src('./environment.yaml')
    // Instead of using gulp-uglify, you can create an inline plugin
    .pipe(through2.obj((file, _, cb) => {
      console.log(file);
      if (file.isBuffer()) {
      }
      console.log(cb);
      cb(null, file);
    }));
};
