FROM node as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./frontend/package.json /app/package.json
COPY ./frontend/tsconfig.json /app/tsconfig.json
RUN npm install --silent
RUN npm install react-scripts -g --silent
COPY ./frontend/src /app/src
COPY ./frontend/public /app/public
RUN npm run build


FROM nginx
COPY --from=build /app/build /usr/share/nginx/html
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
