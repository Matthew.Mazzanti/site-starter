FROM hasura/graphql-engine:v1.0.0-beta.6.cli-migrations
COPY wait-for.sh /bin
RUN chmod +x /bin/wait-for.sh
COPY metadata.json /hasura-migrations/metadata.json

ENTRYPOINT /bin/wait-for.sh postgres:5432 -- /bin/docker-entrypoint.sh graphql-engine serve
