create database test;
\c test;

create extension if not exists "uuid-ossp";
create extension if not exists pgcrypto;

create schema if not exists hdb_catalog;
create schema if not exists hdb_views;

create table users(
    id              uuid primary key default uuid_generate_v4(),
    username        varchar(50) unique not null,
    password        char(60) not null,
    description     text
);

create table roles(
    id              uuid primary key default uuid_generate_v4(),
    name            text not null
);

create table users_roles(
    id              uuid primary key default uuid_generate_v4(),
    user_id         uuid references users(id) not null,
    role_id         uuid references roles(id) not null
);

create table sessions(
    id              uuid primary key default uuid_generate_v4(),
    user_id         uuid references users(id) not null,
    created_at      timestamp default now()
);

create table tasks(
    id              uuid primary key default uuid_generate_v4(),
    name            text not null,
    description     text,
    user_id         uuid references users(id) not null,
    created_at      timestamp default now()
);
