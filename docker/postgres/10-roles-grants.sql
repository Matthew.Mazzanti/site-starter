\c test;

create role hasura with login password 'hasura';
create role node with login password 'node';

alter schema hdb_catalog owner to hasura;
alter schema hdb_views owner to hasura;

grant select on all tables in schema information_schema to hasura;
grant select on all tables in schema pg_catalog to hasura;

grant all on users, roles, users_roles, tasks to hasura, node;
grant all on sessions to node;
