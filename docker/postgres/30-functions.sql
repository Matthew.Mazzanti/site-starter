\c test;

create function check_password(v_user_id uuid, v_password text)
returns boolean as $$
begin
    if (
        select password=crypt(v_password, password) 
        from users
        where users.id=v_user_id
    ) then
        return True;
    else
        return False;
    end if;
end
$$ language plpgsql;

create type user_session as (
    session_id uuid,
    user_id uuid,
    username varchar(50)
);

create function create_session(v_user_id uuid)
returns uuid as $$
declare
    v_session_id uuid;
begin
    insert into sessions(user_id)
        values (v_user_id)
        returning sessions.id into v_session_id;

    return v_session_id;
end
$$ language plpgsql;

create function create_user(v_username text, v_password text) 
returns user_session as $$
declare
    v_user_id uuid;
    v_user_session user_session;
begin
    insert into users(username, password)
        values (v_username, v_password)
        returning users.id into v_user_id;

    v_user_session.session_id = create_session(v_user_id);
    v_user_session.user_id = v_user_id;
    v_user_session.username = v_username;

    return v_user_session;
end
$$ language plpgsql;

create function login(v_username text, v_password text)
returns user_session as $$
declare
    v_user_id uuid;
    v_user_session user_session;
begin
    select id into v_user_id
        from users
        where username=v_username;

    if (v_user_id is not null and check_password(v_user_id, v_password))
    then
        v_user_session.session_id = create_session(v_user_id);
        v_user_session.user_id = v_user_id;
        v_user_session.username = v_username;
    end if;

    return v_user_session;
end
$$ language plpgsql;

create function user_from_session(v_session_id uuid)
returns user_session as $$
declare
    v_user_id uuid;
    v_username text;
    v_user_session user_session;
begin
    select id, username into v_user_id, v_username
        from users where id = (
            select user_id
                from sessions
                where id = v_session_id
        );

    if (v_user_id is not null)
    then
        v_user_session.session_id = v_session_id;
        v_user_session.user_id = v_user_id;
        v_user_session.username = v_username;
    end if;
    return v_user_session;
end
$$ language plpgsql;
