\c test;

insert into users(username, password)
values
    ('foo', 'foobarbaz');

insert into roles(name) 
values
    ('admin'),
    ('user');
