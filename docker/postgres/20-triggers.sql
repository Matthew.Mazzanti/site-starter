\c test;

create function hash_password()
returns trigger as $$
begin
    new.password := crypt(new.password, gen_salt('bf'));
    return new;
end;
$$ language plpgsql;

create trigger hash_user_password
    before insert or update
    on users
    for each row
    execute procedure hash_password();
