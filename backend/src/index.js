import {ApolloServer} from 'apollo-server-hapi';
import Hapi from '@hapi/hapi';
import boom from '@hapi/boom';

import db from './database';

const showUserErrors = (req, h) => {
  // Check if we threw a boom error
  const res = req.response;
  if (!(res && res.isBoom && res.output && res.output.statusCode)) {
    return h.continue
  }

  // If we threw an error in the 400 range, show the message
  const statusCode = res.output.statusCode;
  console.log(statusCode);
  console.log(res.output);
  if (statusCode >= 400 && statusCode < 500 && res.data) {
    res.output.payload.data = res.data;
  }

  return h.continue;
};

const main = async () => {
  const apollo = new ApolloServer({
    typeDefs: `
      type User {
        id: ID!
        username: String!
      }

      type Query {
        currUser: User
      }

      type Mutation {
        createUser(username: String!, password: String!): User!
        login(username: String!, password: String!): User!
        logout: Boolean
      }
    `,
    resolvers: {
      Query: {
        currUser: async (_, __, {user}) => {
          console.log("curr user called");
          if (user) {
            return {
              id: user.user_id,
              username: user.username
            };
          } else {
            return null
          }
        }
      },
      Mutation: {
        createUser: async (_, {username, password}, {user, setCookie}) => {
          if (user) {
            throw boom.badRequest("Cannot create user while logged in");
          }

          // TODO: Catch postgres duplicate key error?
          const { session_id, user_id } = await db.createUser(username, password);
          if(!session_id) {
            throw boom.badRequest("Invalid username");
          }

          setCookie({ id: session_id });
          return { id: user_id, username };
        },
        login: async (_, {username, password}, {user, setCookie}) => {
          if (user) throw boom.badRequest("Already logged in");

          // Find user info
          const { session_id, user_id } = await db.login(username, password);
          if(!session_id) {
            throw boom.badRequest("Invalid username or password");
          }

          // Set user cookie and return user info
          setCookie({ id: session_id });
          return { id: user_id, username };
        },
        logout: async (_, __, {user, clearCookie}) => {
          // If no user or session id, return false as we haven't logged out
          if (!user?.session_id) return false

          // Otherwise log out and clear cookie
          await db.logout(user.session_id);
          clearCookie();
          return true;
        }
      }
    },
    context: ({request, h}) => ({
        user: request.auth.credentials,
        setCookie: (cookie) => { request.cookieAuth.set(cookie) },
        clearCookie: () => { request.cookieAuth.clear() }
    }),
    playground: {
        settings: {
            "request.credentials": "same-origin"
        }
    }
  });

  const server = new Hapi.server({
    host: 'localhost',
    port: 4000,
    routes: {
      validate: {
        options: {
          abortEarly: false,
        },
        failAction: (_, __, err) => {
          console.error(err);
          throw boom.badRequest("Validation failed", err.details);
        }
      },
      cors: {
        origin: ['http://localhost:3000'],
        headers: ["Access-Control-Allow-Headers", "Access-Control-Allow-Origin","Accept", "Authorization", "Content-Type", "If-None-Match", "Accept-language"],
        additionalHeaders: ["Access-Control-Allow-Headers: Origin, Content-Type, x-ms-request-id , Authorization"],
        credentials: true
      }
    }
  });

  server.ext('onPreResponse', showUserErrors);

  await server.register(require('@hapi/cookie'));
  server.auth.strategy('session', 'cookie', {
    cookie: {
      name: 'sid-example',
      password: '!wsYhFA*C2U6nz=Bu^%A@^F#SF3&kSR6',
      isSecure: false,
      clearInvalid: true,
      isSameSite: false,
      isHttpOnly: false
    },
    validateFunc: async (_, session) => {
      console.log("validating");
      const user = await db.userFromSession(session.id);

      if (!user.session_id) {
        return {
          valid: false,
          credentials: null
        };
      }

      return {
        valid: true,
        credentials: user
      };
    }
  });
  server.auth.default('session');

  await apollo.applyMiddleware({
    app: server,
    route: {
      auth: {
        mode: 'optional'
      }
    }
  });
  await apollo.installSubscriptionHandlers(server.listener);

  await db.connect();
  console.log("database connected");

  await server.start();
  console.log('server running at: ' + server.info.uri);
};

main();
