import { Client } from 'pg';

const client = new Client({
  user: 'node',
  host: 'localhost',
  database: 'test',
  password: 'node',
  port: 5432
});

const sql = {
  userFromSession: {
    name: 'user-from-session',
    text: 'select * from user_from_session($1);'
  },
  findUserRoles: {
    name: 'find-roles',
    text: `
      select roles.name
      from users
        inner join users_roles on users.id=users_roles.user_id
        inner join roles on users_roles.role_id=roles.id
      where
        users.id = $1
    `
  },
  createUser: {
    name: 'create-user',
    text: 'select * from create_user($1, $2);'
  },
  login: {
    name: 'login',
    text: 'select * from login($1, $2);'
  },
  logout: {
    name: 'logout',
    text: 'delete from sessions where id=$1'
  }
};

const actions = {
  userFromSession: async (session_id) => {
    const res = await client.query(sql.userFromSession, [session_id]);
    return res.rows[0];
  },
  findRoles: async (user_id) => {
    const res = await client.query(sql.findRoles, [user_id])
    return res.rows.map(row => row.name);
  },
  createUser: async (username, password) => {
    const res = await client.query(sql.createUser, [username, password])
    return res.rows[0];
  },
  login: async (username, password) => {
    const res = await client.query(sql.login, [username, password])
    return res.rows[0];
  },
  logout: async (session_id) => {
    const res = await client.query(sql.logout, [session_id])
    return res.rows[0];
  }
}

export default {
  connect: async () => { await client.connect() },
  ...actions
};
