import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { from } from 'apollo-link';
import gql from 'graphql-tag';
import { query, mutation } from '../queries.js';

const typeDefs = gql`
  extend type Query {
    currUser: User
  }

  extend type Mutation {
    login: Boolean
    logout: Boolean
  }
`;

const resolvers = {
  Query: {
    currUser: async (_, args, { client }) => { 
      console.log("recall");
      const res = await client.query({query: query.serverCurrUser});
      return res.data.currUser;
    }
  },
  Mutation: {
    login: async (_, args, { client, cache }) => {
      console.log(args);
      const res = await client.mutate({
        mutation: mutation.serverLogin,
        variables: {
          username: args.username,
          password: args.password
        }
      });
      cache.writeQuery({
        query: query.currUser,
        data: { currUser: res.data.login }
      });
      return true;
    },
    logout: () => {
      console.log("local logout called");
      return true;
    }
  }
}

const httpLink = new HttpLink({
  uri: 'http://localhost:4000/graphql',
  credentials: 'include',
  //fetchOptions: { mode: 'no-cors' }
});

const errLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, locations, path }) => console.error(
      `[GraphQL error]: Message: ${message}, \
      Location: ${locations}, Path: ${path}`
    ));
  }

  if (networkError) {
    console.error(`[Network error]: ${networkError}`);
  }
});

const link = from([ errLink, httpLink ]);
const cache = new InMemoryCache();
const client = new ApolloClient({ link, cache, typeDefs, resolvers });

export default client;
