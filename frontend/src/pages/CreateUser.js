import React, {useState} from 'react';

import {useMutation} from '@apollo/react-hooks';

import {query, mutation} from '../queries';

const CreateUser = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [saveToken] = useMutation(mutation.saveToken, {
    refetchQueries: ["usersQuery"]
  })
  const [createUser] = useMutation(mutation.createUser, {
    variables: {username, password},
    onCompleted: (data) => {
      if (data && data.createUser && data.createUser.token) {
        saveToken({variables: {token: data.createUser.token}});
      }
    },
    refetchQueries: [{query: query.allUsers}]
  });

  const handleUsername = (e) => {setUsername(e.target.value)};
  const handlePassword = (e) => {setPassword(e.target.value)};
  const handleSubmit = (e) => {
    console.log(e);
    e.preventDefault();
    createUser();
  };

  return <form onSubmit={handleSubmit}>
    <input 
      type="text"
      name="username"
      placeholder="username"
      value={username}
      onChange={handleUsername}
    /><br/>
    <input
      type="text"
      name="password"
      placeholder="password"
      value={password}
      onChange={handlePassword}
    /><br/>
    <input type="submit" value="Create User"/>
  </form>
};

export default CreateUser;
