import React, {useState} from 'react';

import {useMutation} from '@apollo/react-hooks';

import {mutation} from '../queries';

const useFormState = (initial) => {
  const [form, setValues] = useState(initial);
  const formUpdate = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    });
  };
  return [form, formUpdate]
};

const Login = () => {
  const [formState, formUpdate] = useFormState({username: '', password: ''});

  const [login] = useMutation(mutation.login, {
    variables: {
      username: formState.username,
      password: formState.password
    },
  });

  return <form onSubmit={e => {
    e.preventDefault();
    login();
  }}>
    <input
      type="text"
      name="username"
      placeholder="Username"
      value={formState.username}
      onChange={formUpdate}
    />
    <input
      type="password"
      name="password"
      placeholder="Password"
      value={formState.password}
      onChange={formUpdate}
    />
    <input type="submit" value="Login"/>
  </form>
};

export default Login;
