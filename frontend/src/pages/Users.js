import React from 'react';

import {useQuery} from '@apollo/react-hooks';

import {query} from '../queries';

const Users = () => {
  const {loading, error, data} = useQuery(query.allUsers);

  if (loading) return <div>Loading...</div>;
  if (error) {
    console.log(error);
    return <div>Error!</div>;
  };

  if (!data) return <div>No Data returned from query</div>;
  if (!data.users || !data.users.length) return <div>No users</div>;

  return data.users.map(user => <div key={user.id}>{user.username}</div>);
};

export default Users;
