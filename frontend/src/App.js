import React from 'react';

import {ApolloProvider} from '@apollo/react-hooks';
import {useRoutes, A} from 'hookrouter';

import client from './client';
import Login from './pages/Login';
import CurrUser from './components/CurrUser';

const routes = {
  '/login': () => <Login/>
};


const App = () => {
  const routeResult = useRoutes(routes);

  return <ApolloProvider client={client}>
    <CurrUser/>
    <ul>
      <li><A href="/">Home</A></li>
      <li><A href="/login">Login</A></li>
      <li><A href="/create">Create User</A></li>
    </ul>
    {routeResult}
  </ApolloProvider>;
};

export default App;
