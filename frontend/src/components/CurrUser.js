import React from 'react';
import {useQuery} from '@apollo/react-hooks';

import {query} from '../queries';

const CurrUser = () => {
  const {loading, error, data} = useQuery(query.currUser);

  if (loading) return <div>Loading...</div>
  if (error) return <div>Error!</div>;

  if (!data) return <div>No data</div>
  if (!data.currUser) return <div>Not logged in!</div>

  return <div>
    <div>{data.currUser.username}</div>
  </div>
};

export default CurrUser;
