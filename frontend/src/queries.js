import gql from 'graphql-tag';

const query = {
  currUser: gql`
    query currUser {
      currUser @client {
        id
        username
      }
    }
  `,
  serverCurrUser: gql`
    query serverCurrUser {
      currUser {
        id
        username
      }
    }
  `
};

const mutation = {
  login: gql`
    mutation login($username: String!, $password: String!) {
      login(username: $username, password: $password) @client
    }
  `,
  serverLogin: gql`
    mutation serverLogin($username: String!, $password: String!) {
      login(username: $username, password: $password) {
        id
        username
      }
    }
  `
};

export { query, mutation };
