const jsonStorage = {
  getItem: (key, def=null) => {
    let item = localStorage.getItem(key);
    if (item === null) {
      return def;
    } else {
      return JSON.parse(item);
    }
  },
  setItem: (key, item) => {
    localStorage.setItem(key, JSON.stringify(item));
  },
  removeItem: (key) => {
    localStorage.removeItem(key);
  }
};

const saveToken = (token) => {
  jsonStorage.setItem('token', token);
};

export { jsonStorage, saveToken };
